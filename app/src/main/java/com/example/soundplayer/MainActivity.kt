package com.example.soundplayer

import android.media.AudioManager
import android.media.SoundPool
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity() {

    private var soundPool: SoundPool? = null
    private val soundId = 1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        soundPool = SoundPool(12, AudioManager.STREAM_MUSIC, 0)
        soundPool!!.load(baseContext, R.raw.sound1, 1)

        btnPlay.setOnClickListener {playSound(btnPlay)}
    }
    fun playSound(view: View) {
        soundPool?.play(soundId, 1F, 1F, 0, 0, 1F)
        Toast.makeText(this, "Playing sound. . . .", Toast.LENGTH_SHORT).show()
    }
}